﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AddressBookApplication.Startup))]
namespace AddressBookApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
