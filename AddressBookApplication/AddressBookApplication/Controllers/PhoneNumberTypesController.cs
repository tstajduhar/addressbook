﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;

namespace AddressBookApplication.Controllers
{
    public class PhoneNumberTypesController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: PhoneNumberTypes
        public ActionResult Index()
        {
            return View(db.PhoneNumberType.ToList());
        }

        // GET: PhoneNumberTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumberType phoneNumberType = db.PhoneNumberType.Find(id);
            if (phoneNumberType == null)
            {
                return HttpNotFound();
            }
            return View(phoneNumberType);
        }

        // GET: PhoneNumberTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PhoneNumberTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PhoneNumberTypeId,Name")] PhoneNumberType phoneNumberType)
        {
            if (ModelState.IsValid)
            {
                db.PhoneNumberType.Add(phoneNumberType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(phoneNumberType);
        }

        // GET: PhoneNumberTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumberType phoneNumberType = db.PhoneNumberType.Find(id);
            if (phoneNumberType == null)
            {
                return HttpNotFound();
            }
            return View(phoneNumberType);
        }

        // POST: PhoneNumberTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PhoneNumberTypeId,Name")] PhoneNumberType phoneNumberType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phoneNumberType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(phoneNumberType);
        }

        // GET: PhoneNumberTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhoneNumberType phoneNumberType = db.PhoneNumberType.Find(id);
            if (phoneNumberType == null)
            {
                return HttpNotFound();
            }
            return View(phoneNumberType);
        }

        // POST: PhoneNumberTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PhoneNumberType phoneNumberType = db.PhoneNumberType.Find(id);
            db.PhoneNumberType.Remove(phoneNumberType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
