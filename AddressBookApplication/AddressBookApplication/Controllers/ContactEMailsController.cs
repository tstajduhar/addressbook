﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;

namespace AddressBookApplication.Controllers
{
    public class ContactEMailsController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: ContactEMails
        public ActionResult Index()
        {
            var contactEMail = db.ContactEMail.Include(c => c.Contact).Include(c => c.EMailType);
            return View(contactEMail.ToList());
        }

        // GET: ContactEMails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactEMail contactEMail = db.ContactEMail.Find(id);
            if (contactEMail == null)
            {
                return HttpNotFound();
            }
            return View(contactEMail);
        }

        // GET: ContactEMails/Create/1
        public ActionResult Create(int? contactId)
        {
            if (contactId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(contactId);
            if (contact == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContactId = contactId;
            ViewBag.FirstName = contact.FirstName;
            ViewBag.LastName = contact.LastName;
            ViewBag.ContactTitle = contact.Title;
            ViewBag.Company = contact.Company;
            ViewBag.HomeAddress = contact.HomeAddress;
            ViewBag.EMailTypeId = new SelectList(db.EMailType, "EMailTypeId", "Name");
            return View();
        }

        // POST: ContactEMails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EMailId,ContactId,EMailTypeId,EMail,Default")] ContactEMail contactEMail)
        {
            if (ModelState.IsValid)
            {
                if (contactEMail.Default)
                {
                    var existingDefaultEMail = db.ContactEMail.Where(m =>
                        m.ContactId == contactEMail.ContactId
                        && m.Default).FirstOrDefault();

                    if (existingDefaultEMail != null)
                    {
                        existingDefaultEMail.Default = false;
                        db.Entry(existingDefaultEMail).State = EntityState.Modified;
                    }
                }
                else
                {
                    var existingEMail = db.ContactEMail.Where(m =>
                        m.ContactId == contactEMail.ContactId).FirstOrDefault();

                    if (existingEMail == null)
                    {
                        contactEMail.Default = true;
                    }
                }
                db.ContactEMail.Add(contactEMail);
                db.SaveChanges();
                return RedirectToAction("Edit", "Contacts", new { id = contactEMail.ContactId });
            }

            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactEMail.ContactId);
            ViewBag.EMailTypeId = new SelectList(db.EMailType, "EMailTypeId", "Name", contactEMail.EMailTypeId);
            return View();
        }

        // GET: ContactEMails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactEMail contactEMail = db.ContactEMail.Find(id);
            if (contactEMail == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactEMail.ContactId);
            ViewBag.EMailTypeId = new SelectList(db.EMailType, "EMailTypeId", "Name", contactEMail.EMailTypeId);
            return View(contactEMail);
        }

        // POST: ContactEMails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EMailId,ContactId,EMailTypeId,EMail,Default")] ContactEMail contactEMail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactEMail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactEMail.ContactId);
            ViewBag.EMailTypeId = new SelectList(db.EMailType, "EMailTypeId", "Name", contactEMail.EMailTypeId);
            return View(contactEMail);
        }

        // GET: ContactEMails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactEMail contactEMail = db.ContactEMail.Find(id);
            if (contactEMail == null)
            {
                return HttpNotFound();
            }
            return View(contactEMail);
        }

        // POST: ContactEMails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactEMail contactEMail = db.ContactEMail.Find(id);
            db.ContactEMail.Remove(contactEMail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
