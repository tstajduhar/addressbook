﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;
using AddressBookApplication.ViewModels;

namespace AddressBookApplication.Controllers
{
    public class ContactGroupsController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: ContactGroups
        public ActionResult Index()
        {
            return View(db.ContactGroup.ToList());
        }

        // GET: ContactGroups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactGroup contactGroup = db.ContactGroup.Find(id);
            if (contactGroup == null)
            {
                return HttpNotFound();
            }
            return View(contactGroup);
        }

        // GET: ContactGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ContactGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ContactGroupId,Name")] ContactGroup contactGroup)
        {
            if (ModelState.IsValid)
            {
                db.ContactGroup.Add(contactGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contactGroup);
        }

        // GET: ContactGroups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactGroup contactGroup = db.ContactGroup.Find(id);
            if (contactGroup == null)
            {
                return HttpNotFound();
            }
            return View(contactGroup);
        }

        // POST: ContactGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ContactGroupId,Name")] ContactGroup contactGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contactGroup);
        }

        // GET: ContactGroups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactGroup contactGroup = db.ContactGroup.Find(id);
            if (contactGroup == null)
            {
                return HttpNotFound();
            }
            return View(contactGroup);
        }

        // POST: ContactGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactGroup contactGroup = db.ContactGroup.Find(id);
            db.ContactGroup.Remove(contactGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Contacts/AddContactToGroup/1
        public ActionResult AddContactToGroup(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactGroup contactGroup = db.ContactGroup.Find(id);
            if (contactGroup == null)
            {
                return HttpNotFound();
            }

            AddContactToGroupViewModel viewModel = new AddContactToGroupViewModel()
            {
                ContactGroupId = contactGroup.ContactGroupId,
                Name = contactGroup.Name
            };

            var contacts = db.Contact.AsEnumerable().Select(c => new
            {
                ContactId = c.ContactId,
                Description = string.Format("{0} {1}", c.FirstName, c.LastName)
            })
            .ToList();
            ViewBag.ContactId = new SelectList(contacts, "ContactId", "Description");

            return View(viewModel);
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddContactToGroup(AddContactToGroupViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var contactGroup = db.ContactGroup.Where(g => g.ContactGroupId == viewModel.ContactGroupId).FirstOrDefault();
                var contact = db.Contact.Where(g => g.ContactId == viewModel.ContactId).FirstOrDefault();
                contactGroup.Contact.Add(contact);
                db.Entry(contactGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "ContactGroups", new { id = viewModel.ContactGroupId });
            }

            var contacts = db.Contact.Select(c => new
            {
                ContactId = c.ContactId,
                Description = string.Format("{0} {1}", c.FirstName, c.LastName)
            })
            .ToList();
            ViewBag.ContactId = new SelectList(contacts, "ContactId", "Description");

            return View(viewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
