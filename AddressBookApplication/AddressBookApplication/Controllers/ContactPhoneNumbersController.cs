﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;

namespace AddressBookApplication.Controllers
{
    public class ContactPhoneNumbersController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: ContactPhoneNumbers
        public ActionResult Index()
        {
            var contactPhoneNumber = db.ContactPhoneNumber.Include(c => c.Contact).Include(c => c.PhoneNumberType);
            return View(contactPhoneNumber.ToList());
        }

        // GET: ContactPhoneNumbers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactPhoneNumber contactPhoneNumber = db.ContactPhoneNumber.Find(id);
            if (contactPhoneNumber == null)
            {
                return HttpNotFound();
            }
            return View(contactPhoneNumber);
        }

        // GET: ContactPhoneNumbers/Create/1
        public ActionResult Create(int? contactId)
        {
            if (contactId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(contactId);
            if (contact == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContactId = contactId;
            ViewBag.FirstName = contact.FirstName;
            ViewBag.LastName = contact.LastName;
            ViewBag.ContactTitle = contact.Title;
            ViewBag.Company = contact.Company;
            ViewBag.HomeAddress = contact.HomeAddress;
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name");
            return View();
        }

        // POST: ContactPhoneNumbers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PhoneNumberId,ContactId,PhoneNumberTypeId,PhoneNumber,Default")] ContactPhoneNumber contactPhoneNumber)
        {
            if (ModelState.IsValid)
            {
                if (contactPhoneNumber.Default)
                {
                    var existingDefaultNumber = db.ContactPhoneNumber.Where(n =>
                        n.ContactId == contactPhoneNumber.ContactId
                        && n.Default).FirstOrDefault();

                    if (existingDefaultNumber != null)
                    {
                        existingDefaultNumber.Default = false;
                        db.Entry(existingDefaultNumber).State = EntityState.Modified;
                    }
                }
                db.ContactPhoneNumber.Add(contactPhoneNumber);
                db.SaveChanges();
                return RedirectToAction("Edit", "Contacts", new { id = contactPhoneNumber.ContactId });
            }

            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactPhoneNumber.ContactId);
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name", contactPhoneNumber.PhoneNumberTypeId);
            return View();
        }

        // GET: ContactPhoneNumbers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactPhoneNumber contactPhoneNumber = db.ContactPhoneNumber.Find(id);
            if (contactPhoneNumber == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactPhoneNumber.ContactId);
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name", contactPhoneNumber.PhoneNumberTypeId);
            return View(contactPhoneNumber);
        }

        // POST: ContactPhoneNumbers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PhoneNumberId,ContactId,PhoneNumberTypeId,PhoneNumber,Default")] ContactPhoneNumber contactPhoneNumber)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactPhoneNumber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContactId = new SelectList(db.Contact, "ContactId", "FirstName", contactPhoneNumber.ContactId);
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name", contactPhoneNumber.PhoneNumberTypeId);
            return View(contactPhoneNumber);
        }

        // GET: ContactPhoneNumbers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactPhoneNumber contactPhoneNumber = db.ContactPhoneNumber.Find(id);
            if (contactPhoneNumber == null)
            {
                return HttpNotFound();
            }
            return View(contactPhoneNumber);
        }

        // POST: ContactPhoneNumbers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactPhoneNumber contactPhoneNumber = db.ContactPhoneNumber.Find(id);
            db.ContactPhoneNumber.Remove(contactPhoneNumber);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
