﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;

namespace AddressBookApplication.Controllers
{
    public class EMailTypesController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: EMailTypes
        public ActionResult Index()
        {
            return View(db.EMailType.ToList());
        }

        // GET: EMailTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMailType eMailType = db.EMailType.Find(id);
            if (eMailType == null)
            {
                return HttpNotFound();
            }
            return View(eMailType);
        }

        // GET: EMailTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EMailTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EMailTypeId,Name")] EMailType eMailType)
        {
            if (ModelState.IsValid)
            {
                db.EMailType.Add(eMailType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eMailType);
        }

        // GET: EMailTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMailType eMailType = db.EMailType.Find(id);
            if (eMailType == null)
            {
                return HttpNotFound();
            }
            return View(eMailType);
        }

        // POST: EMailTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EMailTypeId,Name")] EMailType eMailType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eMailType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eMailType);
        }

        // GET: EMailTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMailType eMailType = db.EMailType.Find(id);
            if (eMailType == null)
            {
                return HttpNotFound();
            }
            return View(eMailType);
        }

        // POST: EMailTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EMailType eMailType = db.EMailType.Find(id);
            db.EMailType.Remove(eMailType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
