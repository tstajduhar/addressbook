﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AddressBookApplication.Models;
using AddressBookApplication.ViewModels;

namespace AddressBookApplication.Controllers
{
    public class ContactsController : Controller
    {
        private AddressBookDataEntities db = new AddressBookDataEntities();

        // GET: Contacts
        public ActionResult Index()
        {
            return View(db.Contact.ToList());
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name");
            return View();
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ContactId,FirstName,LastName,Title,Company,HomeAddress")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Contact.Add(contact);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(contact);
        }

        // GET: Contacts/CreateContactWithNumber
        public ActionResult CreateContactWithNumber()
        {
            ViewBag.PhoneNumberTypeId = new SelectList(db.PhoneNumberType, "PhoneNumberTypeId", "Name");
            return View();
        }

        // POST: Contacts/CreateContactWithNumber
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateContactWithNumber(CreateContactWithNumberViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Contact contact = new Contact()
                {
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    Title = viewModel.Title,
                    Company = viewModel.Company,
                    HomeAddress = viewModel.HomeAddress
                };

                db.Contact.Add(contact);
                db.SaveChanges();

                ContactPhoneNumber defaultNumber = new ContactPhoneNumber()
                {
                    ContactId = contact.ContactId,
                    PhoneNumberTypeId = viewModel.PhoneNumberTypeId,
                    PhoneNumber = viewModel.PhoneNumber,
                    Default = true
                };

                db.ContactPhoneNumber.Add(defaultNumber);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ContactId,FirstName,LastName,Title,Company,HomeAddress")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contact.Find(id);
            db.Contact.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Contacts/AddGroupToContact/1
        public ActionResult AddGroupToContact(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            AddGroupToContactViewModel viewModel = new AddGroupToContactViewModel()
            {
                ContactId = contact.ContactId,
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Title = contact.Title,
                Company = contact.Company,
                HomeAddress = contact.HomeAddress
            };

            ViewBag.ContactGroupId = new SelectList(db.ContactGroup, "ContactGroupId", "Name");
            return View(viewModel);
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddGroupToContact(AddGroupToContactViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var contactGroup = db.ContactGroup.Where(g => g.ContactGroupId == viewModel.ContactGroupId).FirstOrDefault();
                var contact = db.Contact.Where(g => g.ContactId == viewModel.ContactId).FirstOrDefault();
                contact.ContactGroup.Add(contactGroup);
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "Contacts", new { id = viewModel.ContactId });
            }

            ViewBag.ContactGroupId = new SelectList(db.ContactGroup, "ContactGroupId", "Name");
            return View(viewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
