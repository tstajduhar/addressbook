﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AddressBookApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(EMailType))]
    public partial class EMailType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EMailType()
        {
            this.ContactEMail = new HashSet<ContactEMail>();
        }


        [DisplayName("ID vrste e-mail adrese")]
        public int EMailTypeId { get; set; }
        [Required(ErrorMessage = "Obvezan podatak!")]
        [StringLength(50, ErrorMessage = "Tekst ne smije biti duži od 50 znakova!")]
        [DisplayName("Naziv")]
        public string Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactEMail> ContactEMail { get; set; }
    }
}
