﻿using AddressBookApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AddressBookApplication.ViewModels
{
    [MetadataType(typeof(AddContactToGroupViewModel))]
    public class AddContactToGroupViewModel
    {
        [DisplayName("ID grupe kontakata")]
        public int ContactGroupId { get; set; }
        [DisplayName("Naziv grupe")]
        public string Name { get; set; }
        [DisplayName("ID kontakta")]
        public int ContactId { get; set; }
    }
}