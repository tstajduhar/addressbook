﻿using AddressBookApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AddressBookApplication.ViewModels
{
    [MetadataType(typeof(AddGroupToContactViewModel))]
    public class AddGroupToContactViewModel
    {
        [DisplayName("ID kontakta")]
        public int ContactId { get; set; }
        [DisplayName("Ime")]
        public string FirstName { get; set; }
        [DisplayName("Prezime")]
        public string LastName { get; set; }
        [DisplayName("Titula")]
        public string Title { get; set; }
        [DisplayName("Kompanija")]
        public string Company { get; set; }
        [DisplayName("Kućna adresa")]
        public string HomeAddress { get; set; }
        [DisplayName("ID grupe kontakata")]
        public int ContactGroupId { get; set; }
    }
}