﻿using AddressBookApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AddressBookApplication.ViewModels
{
    [MetadataType(typeof(CreateContactWithNumberViewModel))]
    public class CreateContactWithNumberViewModel
    {
        [DisplayName("ID kontakta")]
        public int ContactId { get; set; }
        [Required(ErrorMessage = "Obvezan podatak!")]
        [StringLength(50, ErrorMessage = "Tekst ne smije biti duži od 50 znakova!")]
        [DisplayName("Ime")]
        public string FirstName { get; set; }
        [StringLength(50, ErrorMessage = "Tekst ne smije biti duži od 50 znakova!")]
        [DisplayName("Prezime")]
        public string LastName { get; set; }
        [StringLength(50, ErrorMessage = "Tekst ne smije biti duži od 50 znakova!")]
        [DisplayName("Titula")]
        public string Title { get; set; }
        [StringLength(100, ErrorMessage = "Tekst ne smije biti duži od 100 znakova!")]
        [DisplayName("Kompanija")]
        public string Company { get; set; }
        [StringLength(300, ErrorMessage = "Tekst ne smije biti duži od 300 znakova!")]
        [DisplayName("Kućna adresa")]
        public string HomeAddress { get; set; }
        [Required(ErrorMessage = "Obvezan podatak!")]
        [DisplayName("ID vrste telefonskog broja")]
        public int PhoneNumberTypeId { get; set; }
        [Required(ErrorMessage = "Obvezan podatak!")]
        [StringLength(25, ErrorMessage = "Tekst ne smije biti duži od 25 znakova!")]
        [Phone(ErrorMessage = "Telefonski broj nije valjan!")]
        [DisplayName("Telefonski broj")]
        public string PhoneNumber { get; set; }
    }
}