﻿CREATE TABLE [dbo].[ContactPhoneNumber]
(
	[PhoneNumberId] INT IDENTITY (1, 1) NOT NULL,
	[ContactId] INT NOT NULL,
	[PhoneNumberTypeId] INT NOT NULL,
	[PhoneNumber] VARCHAR(25) NOT NULL,
    [Default] BIT NOT NULL, 
    CONSTRAINT [PK_ContactPhoneNumber] PRIMARY KEY (PhoneNumberId),
	CONSTRAINT [FK_ContactPhoneNumber_Contact] FOREIGN KEY ([ContactId])
        REFERENCES [dbo].[Contact] ([ContactId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContactPhoneNumber_PhoneNumberType] FOREIGN KEY ([PhoneNumberTypeId])
        REFERENCES [dbo].[PhoneNumberType] ([PhoneNumberTypeId]) ON DELETE CASCADE
)
