﻿CREATE TABLE [dbo].[ContactEMail]
(
	[EMailId] INT IDENTITY (1, 1) NOT NULL,
	[ContactId] INT NOT NULL,
	[EMailTypeId] INT NOT NULL,
	[EMail] VARCHAR(254) NOT NULL,
    [Default] BIT NOT NULL, 
    CONSTRAINT [PK_ContactEMail] PRIMARY KEY (EMailId),
	CONSTRAINT [FK_ContactEMail_Contact] FOREIGN KEY ([ContactId])
        REFERENCES [dbo].[Contact] ([ContactId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContactEMail_EMailType] FOREIGN KEY ([EMailTypeId])
        REFERENCES [dbo].[EMailType] ([EMailTypeId]) ON DELETE CASCADE
)
