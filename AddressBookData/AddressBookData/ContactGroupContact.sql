﻿CREATE TABLE [dbo].[ContactGroupContact]
(
	[ContactGroupId] INT NOT NULL,
	[ContactId] INT NOT NULL,
    CONSTRAINT [PK_ContactGroupContact] PRIMARY KEY (ContactGroupId, ContactId),
    CONSTRAINT [FK_ContactGroupContact_ContactGroup] FOREIGN KEY ([ContactGroupId])
        REFERENCES [dbo].[ContactGroup] ([ContactGroupId]) ON DELETE CASCADE,
	CONSTRAINT [FK_ContactGroupContact_Contact] FOREIGN KEY ([ContactId])
        REFERENCES [dbo].[Contact] ([ContactId]) ON DELETE CASCADE
)
