﻿MERGE INTO PhoneNumberType AS TARGET 
USING (VALUES 
	(1, 'Mobilni'), 
	(2, 'Kućni'),
	(3, 'Poslovni'),
	(4, 'Ostalo')
) 
AS SOURCE (PhoneNumberTypeId, Name) 
ON TARGET.PhoneNumberTypeId = SOURCE.PhoneNumberTypeId 
WHEN NOT MATCHED BY TARGET THEN
INSERT (Name) 
VALUES (Name);

MERGE INTO EMailType AS TARGET
USING (VALUES
	(1, 'Osobni'), 
	(2, 'Poslovni'),
	(3, 'Ostalo')
)
AS SOURCE (EMailTypeId, Name)
ON TARGET.EMailTypeId = SOURCE.EMailTypeId
WHEN NOT MATCHED BY TARGET THEN
INSERT (Name) 
VALUES (Name);