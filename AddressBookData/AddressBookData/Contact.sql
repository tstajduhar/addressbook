﻿CREATE TABLE [dbo].[Contact]
(
	[ContactId] INT IDENTITY (1, 1) NOT NULL,
	[FirstName] NVARCHAR(50) NOT NULL,
	[LastName] NVARCHAR(50) NULL,
	[Title] NVARCHAR(50) NULL,
	[Company] NVARCHAR(100) NULL,
	[HomeAddress] NVARCHAR(300) NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY (ContactId)
)
