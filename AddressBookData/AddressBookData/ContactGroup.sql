﻿CREATE TABLE [dbo].[ContactGroup]
(
	[ContactGroupId] INT IDENTITY (1, 1) NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_ContactGroup] PRIMARY KEY (ContactGroupId)
)
